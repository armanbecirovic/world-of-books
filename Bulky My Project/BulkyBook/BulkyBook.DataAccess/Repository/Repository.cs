﻿using BulkyBook.DataAccess.Repository.IRepository;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace BulkyBook.DataAccess.Repository
{
    // REPOSITORY PATTERN - As the requirement changes we will be making modificaitons here, and when we make those
    // modif. in this repository, all the classes that implement this repo. will be updated as well.
    public class Repository<T> : IRepository<T> where T : class
    {
        private readonly ApplicationDbContext _db;
        internal DbSet<T> dbSet;
        public Repository(ApplicationDbContext db)
        {
            _db = db;            
            this.dbSet = _db.Set<T>(); // creating DbSet for the given generic T type
            // this.dbSet = _db.Categories  (T is type, in this case Category class, type)
        }

        public void Add(T entity)
        {
            dbSet.Add(entity);
        }

        // Expected parameter includeProp - "Category,CoverType"
        public IEnumerable<T> GetAll(Expression<Func<T, bool>>? filter = null, string? includeProperties = null)
        {
            IQueryable<T> query = dbSet;

            if (filter != null)
            {
                query = query.Where(filter);    // filtering data from query
            }

            if (includeProperties != null)
            {
                foreach (var includeProp in includeProperties.Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries))
                {
                    query = query.Include(includeProp);
                    // same as: _db.ShoppingCarts.Include(u => u.Product)
                }

            }

            return query.ToList();
        }

        public T GetFirstOrDefault(Expression<Func<T, bool>> filter, string? includeProperties = null, bool tracked = true)
        {
            IQueryable<T> query;

            if (tracked)
            {
                query = dbSet;
            }
            else 
            {
                query = dbSet.AsNoTracking();   // same as -> _db.ShoppingCarts.AsNoTracking()
            }

            query = query.Where(filter);    // filtering data from query
            if (includeProperties != null)
            {
                foreach (var includeProp in includeProperties.Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries))
                {
                    query = query.Include(includeProp);
                }

            }

            return query.FirstOrDefault();  // then getting entity we need.
        }

        public void Remove(T entity)
        {
            dbSet.Remove(entity);
        }

        public void RemoveRange(IEnumerable<T> entity)
        {
            dbSet.RemoveRange(entity);
        }
    }
}
