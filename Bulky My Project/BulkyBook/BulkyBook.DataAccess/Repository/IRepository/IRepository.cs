﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace BulkyBook.DataAccess.Repository.IRepository
{
    public interface IRepository<T> where T : class
    {
        // implementing common methods in this interface for generic type
        // T - Category Model or other models

        // method for retrieving entitity from DB ,
        // var categoryFromDbFirst = _db.Categories.FirstOrDefault(c => c.Id == id) ili (c=> c.Name == name) expression
        T GetFirstOrDefault(Expression<Func<T, bool>> filter, string? includeProperties = null, bool tracked = true);

        // get all categories, ienumerable
        // IEnumerable<Category> objCategoryList = _db.Categories;
        IEnumerable<T> GetAll(Expression<Func<T, bool>>? filter = null, string? includeProperties = null);
        // adding category
        void Add(T entity);
        // removing category
        void Remove(T entity);
        // removing more entities from category or other table
        void RemoveRange(IEnumerable<T> entity);

    }
}
