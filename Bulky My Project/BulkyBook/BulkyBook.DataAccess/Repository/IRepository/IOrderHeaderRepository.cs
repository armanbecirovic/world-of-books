﻿using BulkyBook.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BulkyBook.DataAccess.Repository.IRepository
{
    public interface IOrderHeaderRepository : IRepository<OrderHeader>
    {
        void Update(OrderHeader obj);  // specific method that isn't implemented in Repository class that implements
                                       // IRepository interface

        void UpdateStatus(int id, string orderStatus, string? paymentStatus = null);

        void UpdateStripeSessionId(int id, string sessionId);

        void UpdateStripePaymentIntentId(int id, string paymentIntentId);

    }
}
