﻿using BulkyBook.DataAccess.Repository.IRepository;
using BulkyBook.Models;
using BulkyBook.Models.ViewModels;
using BulkyBook.Utility;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;

namespace BulkyBookWeb.Areas.Admin.Controllers
{
    [Area("Admin")]
    [Authorize(Roles = SD.Role_Admin)]

    public class ProductController : Controller
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IWebHostEnvironment _hostEnvironment;

        public ProductController(IUnitOfWork unitOfWork, IWebHostEnvironment hostEnvironment)
        {
            _unitOfWork = unitOfWork;
            _hostEnvironment = hostEnvironment;
        }
        public IActionResult Index()
        {

            return View();
        }

        // GET
        public IActionResult Upsert(int? id)
        {
            TempData["success"] = null;

            ProductVM productVM = new()
            {
                Product = new(),
                CategoryDropDownList = _unitOfWork.Category.GetAll().Select(i => new SelectListItem
                {
                    Text = i.Name,
                    Value = i.Id.ToString()
                }),
                CoverTypeDropDownList = _unitOfWork.CoverType.GetAll().Select(i => new SelectListItem
                {
                    Text = i.Name,
                    Value = i.Id.ToString()
                }),
            };

            if (id == null || id == 0)
            {
                //// create new Product
                //ViewBag.CategoryListDropDown = CategoryListDropDown;
                //ViewData["CoverTypeDropDown"] = CoverTypeDropDown;
                return View(productVM);
            }
            else
            {
                // update Product View
                productVM.Product = _unitOfWork.Product.GetFirstOrDefault(u => u.Id == id);
                return View(productVM);
            }

        }

        // POST
        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult Upsert(ProductVM obj, IFormFile? file)     // proveriti slanja fajla, slike pomocu Debug
        {                                               // npr. kada zelimo da promenimo sliku, odaberemo novu.
            if (ModelState.IsValid)
            {
                string wwwRootPath = _hostEnvironment.WebRootPath;

                if (file != null)
                {
                    // file is uploaded
                    string fileName = Guid.NewGuid().ToString();    // new,random Image name
                    var path = Path.Combine(wwwRootPath, @"images\products");   // Path where to save Image
                    var extension = Path.GetExtension(file.FileName);   // Image file extension

                    // checking if old image exists? Delete if its true.
                    if (obj.Product.ImageUrl != null) 
                    {
                        var oldImagePath = Path.Combine(wwwRootPath, obj.Product.ImageUrl.TrimStart('\\'));
                        if (System.IO.File.Exists(oldImagePath)) 
                        {
                            System.IO.File.Delete(oldImagePath);
                        }
                    }

                    // copying the file that was uploaded inside product folder
                    // final path where the file needs to be uploaded
                    using (var fileStreams = new FileStream(Path.Combine(path, fileName + extension), FileMode.Create))
                    {
                        file.CopyTo(fileStreams);
                    }
                    // update imageUrl!
                    obj.Product.ImageUrl = @"\images\products\" + fileName + extension;

                }
                if (obj.Product.Id == 0)
                {
                    _unitOfWork.Product.Add(obj.Product);                    
                    TempData["success"] = "Product created succesfully!";
                }
                else 
                {
                    _unitOfWork.Product.Update(obj.Product);
                    TempData["success"] = "Product updated succesfully!";
                }

                _unitOfWork.Save();
                return RedirectToAction("Index");
            }

            return View(obj);
        }                

        #region API CALLS - ENDPOINTS
        // Retrieve all Products
        [HttpGet]
        public IActionResult GetAll()
        {
            var productList = _unitOfWork.Product.GetAll(includeProperties: "Category,CoverType");
            return Json(new { data = productList });
        }


        // Post Delete
        [HttpDelete]
        public IActionResult Delete(int? id)
        {
            var obj = _unitOfWork.Product.GetFirstOrDefault(c => c.Id == id);

            if (obj == null)
            {
                return Json(new { success = false, message = "Error while deleting!" });
            }
            // delete Image
            var imagePath = Path.Combine(_hostEnvironment.WebRootPath, obj.ImageUrl.TrimStart('\\'));
            if (System.IO.File.Exists(imagePath)) 
            {
                System.IO.File.Delete(imagePath);
            }
            
            _unitOfWork.Product.Remove(obj);
            _unitOfWork.Save();

            return Json(new { success = true, message = "Product deleted succesfully!"});
        }
        #endregion
    }
}
