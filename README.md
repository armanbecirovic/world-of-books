# World of Books - Web based project

## 1) PROJECT - Bulky Book

### Built with
* ASP.NET MVC Core (.NET 6)
* Entity Framework Core
* Bootstrap v5
* HTML
* CSS 
* jQuery

### Included
* Repository and Unit of Work patterns
* N-Tier Architecture
* Role Management in ASP.NET Core Identity(Identity UI Razor class libraries, Razor Pages)
* Authentication, Authorization
* Facebook Authentication/Login
* Stripe Payment Integrations
* Email notifications(Verify email adress, receive Order informations)
* Database Migrations(Seed DB Migrations and test data automatically)

### About
This project is about Book Web Store where customers can choose and make an order to buy desired book. Besides that, they can see relevant informations about books, check the different prices for each book(depending on quantity) and make safe payments after ordering desired product. Roles of a user are: Admin, Employee, Company and Individual user. Depending on role, user is able to perform different/authorized actions. For example, Admins and Employees, unlike Individual users(Customers), can perform CRUD operations(create Category or Cover Type of specific book, Company). Also, Company and Individual customers can check their Shopping cart and see its content in any time.

### Getting Started - Prerequisites
Visual Studio 2022, Microsoft SQL Server Management Studio

1. In _appsettings.json_ Edit Server and Database names with appropriate ones
```bash
"DefaultConnection": "Server=DESKTOP-CT2NGTJ;Database=Bulky3;Trusted_Connection=True;"
```
2. Open Package Manager Console and type:
```sh
   update-database
   ```
New Database should be created. Check it for in Microsoft SSMS.
3. For Stripe payments edit Secret and Publishable Keys(Stripe account is needed) - https://stripe.com/
```bash
"Stripe": {
    "SecretKey": ""
    "PublishableKey": ""
}
```
4. Facebook Authentication/Login - Set up new app and edit AppId and AppSecret with appropriate ones(Sing in with FB profile https://developers.facebook.com/)
Program.cs 
```bash
builder.Services.AddAuthentication().AddFacebook(options =>
{
    options.AppId = "";
    options.AppSecret = "";
});
```
5. Make sure _BulkyBookWeb_ project is set as Startup project
6. Run the project.

### Additional info
Database seeding with an initial set of data is done in _DbInitializer.cs_ file.
One Admin and Individual user with their credentials(along with Identity Roles) are populated there as well. 
Initial Users and their credentials: 
1. Admin (headAdmin@gmail.com, Admin1.)
2. Individual User (user1@gmail.com, User1.)
- Admins can register new Users and assign roles.


## 2) PROJECT - Abby

Small ASP.NET Core Razor application with CRUD operations with EF Core for integration with the database. 
In future it will be expanded and built similar to previously described MVC project.

## Contact

Arman Becirovic
- Email: armanbecirovic1@gmail.com
- LinkedIn: https://www.linkedin.com/in/armanbecirovic/

Project Link: https://gitlab.com/armanbecirovic/world-of-books.git

## Credits 

Special thanks to Bhrugen Patel with his very useful and comprehensive tutorial - https://www.udemy.com/course/complete-aspnet-core-21-course/
